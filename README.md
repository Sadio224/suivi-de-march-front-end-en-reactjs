Ce projet constitue la partie "Front-end" du projet suivi de marché du module JEE, 

Il a été dévéloppé avec ReactJs.

## Dependances : 
    -ReactJs
    -NodeJs
    -npm

## Demarrage : 

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

app/docs/troubleshooting#npm-run-build-fails-to-minify

## Video Demonstration : 