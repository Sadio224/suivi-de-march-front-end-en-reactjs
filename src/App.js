import React, {Component} from 'react';
import './App.css';

import Services from './Components/Pages/Services';
import Personnels from './Components/Pages/Personnels'
import ServiceAdd from './Components/Service';
import ServiceId from './Components/ByService/ByService';
import NavBar from './Components/Helpers/AppNav';

import { Route, BrowserRouter as Router,Switch} from 'react-router-dom'

class App extends Component  {
  render() { 
    return ( 
      <div>
        <NavBar/>
        <Router>
            <Switch>
                 <Route path='/' exact={true} component={ServiceAdd}/>
                 <Route path='/service' exact={true} component={ServiceAdd}/>
                 <Route path='/services' exact={true} component={Services}/>
                 <Route path='/personnels' exact={true} component={Personnels}/>
                 <Route path='/services/:id' exact={true} component={ServiceId}/>
            </Switch>
         </Router>
      </div>
    );
  }
}

export default App;
