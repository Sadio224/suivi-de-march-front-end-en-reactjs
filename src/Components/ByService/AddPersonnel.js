import React, { Component } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter,Label, FormGroup, Form } from 'reactstrap';
import  'bootstrap/dist/css/bootstrap.min.css' 
import '../Modal2.css'

class MyModalPers extends Component{


    emptyItem = {
        nom : '' ,
        prenom : '',
        profession : '',
        service: {
            id_service : this.props.idService,
            name_service : "Informatique"
        }
    }

    constructor(props){
        super(props)

        this.state = { 
          isLoading :false,
          item : this.emptyItem,
          modal : false,
          nestedModal : false,
          closeAll : false
        }
  
         this.handleSubmit= this.handleSubmit.bind(this);
         this.handleChange= this.handleChange.bind(this);
         this.toggleAll=this.toggleAll.bind(this);
         this.toggle=this.toggle.bind(this);
         this.toggleNested=this.toggleNested.bind(this);
    } 

    
      handleChange(event){
        const target= event.target;
        const value= target.value;
        const name = target.name;
        let item={...this.state.item};
        item[name] = value;
        this.setState({item});
        console.log(item);
      }

    async handleSubmit(event){
     
        const item = this.state.item;
      
  
        await fetch(`/api/personnel`, {
          method : 'POST',
          headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
          },
          body : JSON.stringify(item),
        });
        
        event.peventDefault();
        this.props.history.push(`/services/${this.props.idService}`);
    }

    toggle(event){
        const modal = this.state.modal;
        this.setState({modal : !modal});
    } 

    toggleNested(event){
        const nestedModal = this.state.nestedModal;
        this.setState({nestedModal : !nestedModal, closeAll : false });
    }

    toggleAll(event){
        const nestedModal = this.state.nestedModal;
        this.setState({nestedModal : !nestedModal, closeAll : true });
    }


    render(){

        return(
            <div>
                <span size="sm" color="primary" onClick={this.toggle}><span style={{color:'red', fontSize:'20px', fontWeight:'800'}}>+</span>{this.props.name}</span>
                <Modal isOpen={this.state.modal} toggle={this.toggle} className="col-md-10">
                    <ModalHeader toggle={this.toggle}>Add Personnal</ModalHeader>
                    <ModalBody>

                        <Form onSubmit={this.handleSubmit}>
                            <FormGroup>
                                <Label for="nom">Nom</Label>
                                <input className="input" type="nom" name="nom" id="nom" onChange={this.handleChange} autoComplete="name"/>
                            </FormGroup>
                            <FormGroup>
                                <Label for="prenom">Prenom</Label>
                                <input className="input" type="prenom" name="prenom" id="prenom" onChange={this.handleChange} autoComplete="name"/>
                            </FormGroup>
                            
                            <FormGroup>
                                <Label for="profession">Profession</Label>
                                <input className="input" type="profession" name="profession" id="profession" onChange={this.handleChange} autoComplete="name"/>
                            </FormGroup>
                            
                            <FormGroup>
                                <Button color="primary" type="submit">Save</Button>{' '}
                                
                            </FormGroup>
                        </Form>

                         <br />
                    
                    </ModalBody>
                    <ModalFooter>
                        
                        <Button color="secondary" onClick={this.toggle}>Cancel</Button>
                    </ModalFooter>
                </Modal>
            </div>
        );
    }
}

export default MyModalPers