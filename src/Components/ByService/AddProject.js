import React, { Component } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter,Label, FormGroup, Form } from 'reactstrap';
import  'bootstrap/dist/css/bootstrap.min.css' 
import '../Modal2.css'
import DatePicker from 'react-datepicker';
import "react-datepicker/dist/react-datepicker.css";

class MyModal extends Component{


    emptyItem = {
        objectif : '' ,
        datedebut : new Date(),
        datefin : new Date(),
        budget : '',
        etat : 'invalide',
        serviceE: {
            id_service : this.props.idService,
            name_service : "Informatique"
        }
    }

    constructor(props){
        super(props)

        this.state = { 
          isLoading :false,
          Services : [],
          date :new Date(),
          item : this.emptyItem,
          modal : false,
          nestedModal : false,
          closeAll : false
        }
  
         this.handleSubmit= this.handleSubmit.bind(this);
         this.handleChange= this.handleChange.bind(this);
         this.toggleAll=this.toggleAll.bind(this);
         this.toggle=this.toggle.bind(this);
         this.toggleNested=this.toggleNested.bind(this);
         this.handleDateChangeDebut= this.handleDateChangeDebut.bind(this);
         this.handleDateChangeFin= this.handleDateChangeFin.bind(this);
         this.handleChangeService=this.handleChangeService.bind(this);
    } 

    
      handleChange(event){
        const target= event.target;
        const value= target.value;
        const name = target.name;
        let item={...this.state.item};
        item[name] = value;
        this.setState({item});
        console.log(item);
      }

      handleChangeService(event){
        const target= event.target;
        const value= target.value;
        const name = target.name;
        let item={...this.state.item};
        item[name].id_service = this.props.idService;
        this.setState({item});
        console.log(item);
      }

      handleDateChangeDebut(date){
        let item={...this.state.item};
        item.datedebut= date;
        this.setState({item});
      
      }

      handleDateChangeFin(date){
        let item={...this.state.item};
        item.datefin= date;
        this.setState({item});
      
      }

    async handleSubmit(event){
     
        const item = this.state.item;
      
  
        await fetch(`/api/projet`, {
          method : 'POST',
          headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
          },
          body : JSON.stringify(item),
        });
        
        event.peventDefault();
        this.props.history.push(`/services/${this.props.idService}`);
    }

    toggle(event){
        const modal = this.state.modal;
        this.setState({modal : !modal});
    } 

    toggleNested(event){
        const nestedModal = this.state.nestedModal;
        this.setState({nestedModal : !nestedModal, closeAll : false });
    }

    toggleAll(event){
        const nestedModal = this.state.nestedModal;
        this.setState({nestedModal : !nestedModal, closeAll : true });
    }


    render(){

        return(
            <div>
                <span size="sm" color="primary" onClick={this.toggle}><span style={{color:'red', fontSize:'20px', fontWeight:'800'}}>+</span>{this.props.name}</span>
                <Modal isOpen={this.state.modal} toggle={this.toggle} className="col-md-10">
                    <ModalHeader toggle={this.toggle}>Add project</ModalHeader>
                    <ModalBody>

                        <Form onSubmit={this.handleSubmit}>
                            <FormGroup>
                                <Label for="objectif">Objectif</Label>
                                <input className="input" type="objectif" name="objectif" id="objectif" onChange={this.handleChange} autoComplete="name"/>
                            </FormGroup>
                            <FormGroup>
                                <Label for="city">Date debut</Label>
                                <DatePicker selected={this.state.item.datedebut}  onChange={this.handleDateChangeDebut} />
                            </FormGroup>
                            <FormGroup>
                                <Label for="city">Date fin</Label>
                                <DatePicker selected={this.state.item.datefin}  onChange={this.handleDateChangeFin} />
                            </FormGroup>
                            <FormGroup>
                                <Label for="budget">Budget</Label>
                                <input className="input" type="budget" name="budget" id="budget" onChange={this.handleChange} autoComplete="name"/>
                            </FormGroup>
                            
                            <FormGroup>
                                <Button color="primary" type="submit">Save</Button>{' '}
                               
                            </FormGroup>
                        </Form>

                    <br />
                   
                    
                    </ModalBody>
                    <ModalFooter>
                        <Button color="secondary" onClick={this.toggle}>Cancel</Button>
                    </ModalFooter>
                </Modal>
            </div>
        );
    }
}

export default MyModal