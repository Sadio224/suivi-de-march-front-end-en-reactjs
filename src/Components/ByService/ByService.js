import React, { Component } from 'react';
import  'bootstrap/dist/css/bootstrap.min.css' ;
import {Badge, Table,Container,Button} from 'reactstrap';
import '../ServiceAdd.css';
import '../ServiceId.css';
import {Link} from 'react-router-dom';
import MyModal from './AddProject'
import MyModalPers from '../ByService/AddPersonnel'
import ModalTache from '../Tache/Tache'
import Breadcrumbs from '../Helpers/Breadcrumbs';
import ButtonUi from '@material-ui/core/Button';

import StandaloneToggleButton from '../Helpers/ActiveDesactiveProject'


class ServiceId extends Component {

    constructor(props){
        super(props)
  
        this.state = { 
            id_service : parseInt(this.extratIdService(this.props.location.pathname),10),
            service : {},
            Projets : [],
            Personnals : [],
            Taches : [],
            nameButton : 'Show personals',
            count : 0,
            ok : true,
            selected : true
         }
  
         this.extratIdService= this.extratIdService.bind(this);
    } 

    async remove(id){
        await fetch(`/api/projet/${id}` , {
          method: 'DELETE' ,
          headers : {
            'Accept' : 'application/json',
            'Content-Type' : 'application/json'
          }

        }).then(() => {
          let updatedProjects = [...this.state.Projets].filter(i => i.id !== id);
          this.setState({Projets : updatedProjects});
        });

        window.location.reload(false);
    }

    async removePersonnal(id){
        await fetch(`/api/personnel/${id}` , {
          method: 'DELETE' ,
          headers : {
            'Accept' : 'application/json',
            'Content-Type' : 'application/json'
          }

        }).then(() => {
          let updatedPersonnals = [...this.state.Personnals].filter(i => i.id !== id);
          this.setState({Personnals : updatedPersonnals});
        });
    }


    extratIdService(chaineId){
        chaineId = chaineId.trim();
        chaineId = chaineId.split('/');
        const result = chaineId[2];
        return result;
    }

    handleButtonChange = (e) => {
        const {Projets, Personnals, ok} = this.state;
        if(ok)
            this.setState({nameButton : "Show projects", ok : false, count : Projets.length});
        else
        this.setState({nameButton : "Show personnals", ok : true, count : Personnals.length}); 
    }

    handleInvalideChange() {
        const {selected} = this.state
        this.setState({selected : !selected})
    }

    async componentDidMount() {
        
        const vara = this.props.location.pathname;
        const id = this.extratIdService(vara);
        const response= await fetch(`/api/service/${id}`);
        const body= await response.json();
        this.setState({id_service : id, service : body});

        const responseProj= await fetch('/api/projets');
        const bodyProj= await responseProj.json();
        let updatedProjects = [...bodyProj].filter(i =>(
            i.serviceE.id_service === Number(this.state.id_service)));
        this.setState({Projets : updatedProjects});

        const responsePers = await fetch('/api/personnels');
        const bodyPers = await responsePers.json();
        let updatedPersonnals = [...bodyPers].filter(i =>(
            i.service.id_service === Number(this.state.id_service)));
        this.setState({Personnals : updatedPersonnals, count : updatedPersonnals.length});

        const responseTache = await fetch('/api/taches');
        const bodyTache = await responseTache.json();
        
        this.setState({Taches : bodyTache});

    }

    CountTache(id_projet){
        const { Taches } = this.state
        let updateTaches = [...Taches].filter(i => (
            i.projet.id_projet === Number(id_projet)
        ));
        return updateTaches.length
    }

    render() { 
        
        const {id_service, Projets, Personnals} = this.state;

        let rows=
        Projets.map( project =>
              <tr key={project.id_projet}>
                <td><Link><ModalTache name={project.objectif} idProject={project.id_projet} nameProject={project.objectif}/></Link></td>
                <td>{project.datedebut}</td>
                <td>{project.datefin}</td>
                <td>{project.budget}</td>
                <td>{project.etat ==="invalide" ? <Badge color="danger">{project.etat}</Badge> : <Badge color="success">valide</Badge> } </td>
                <td><Badge color="secondary">Tache<Badge color="primary">
                    {this.CountTache(project.id_projet)}
                </Badge></Badge></td>
                <td>
                    <Button className="button" color="primary" onClick={() => this.update(project.id_projet)}>Update</Button>
                    <Button className="button" size="sm" color="danger" onClick={() => this.remove(project.id_projet)}>Delete</Button>
                </td>
                <td>
                   <StandaloneToggleButton idProject={project.id_projet} selected={this.state.selected} onChange={()=>this.handleInvalideChange()}/>
                </td>
              </tr>
            )

            let rowsPers=
                Personnals.map( personnal =>
                  <tr key={personnal.id_personnel}>
                    <td>{personnal.nom}</td>
                    <td>{personnal.prenom}</td>
                    <td>{personnal.profession}</td>
                    <td>{personnal.service.name_service}</td>
                    <td>
                        <Button className="button" color="primary" onClick={() => this.uptdatePersonnal(personnal.id_personnel)}>Update</Button>
                        <Button className="button" size="sm" color="danger" onClick={() => this.removePersonnal(personnal.id_personnel)}>Delete</Button>
                    </td>
                  </tr>
                )
            let thPers= (
                <tr>
                    <th width="20%">Nom</th>
                    <th width="25%">Prenom</th>
                    <th width="15%">Profession</th>
                    <th width="20%">Service</th>
                    <th width="20%">Action</th>
                </tr>
            )
                

        return (
            <div>  
                <Breadcrumbs name={this.state.service.name_service}/>
                <Container> 
                    <div className="div"><Link><MyModal idService={id_service} nameService={this.state.service.name_service} name="Add project"/></Link></div>
                    <div className="div"><Link><MyModalPers idService={id_service} nameService={this.state.service.name_service} name="Add personnal"/></Link></div>
                </Container>
                
                {''}
                <Container>
                    <Button className="bouton-droit" size="sm" outline color="primary" onClick={(e) => this.handleButtonChange(e)} >{this.state.nameButton}{' '}<Badge color="secondary">{this.state.count}</Badge></Button>
                    {
                        this.state.ok ? (<div>
                            <h3>Project List</h3>
                            <Table className="mt-4">
                                <thead>
                                    <tr>
                                        <th width="15%">Objectif</th>
                                        <th width="15%">Date debut</th>
                                        <th width="15%">Date fin</th>
                                        <th width="10%">Budget</th>
                                        <th width="10%">Etat</th>
                                        <th width="15%">Tache</th>
                                        <th width="10%">Action</th>
                                        <th width="10%">Activé</th>
                                    </tr>
                                </thead>
                                <tbody>
                                   {rows}
                                </tbody>
                            </Table>
                        </div>) : (
                                <div>
                                    <h3>Personnal List</h3>
                                    <Table className="mt-4">
                                        <thead>
                                            {thPers}
                                        </thead>
                                        <tbody>
                                            {rowsPers}
                                        </tbody>
                                    </Table>
                                </div>
                        )
                    }
                    
                </Container>
            </div>
        );
    }
}

export default ServiceId;