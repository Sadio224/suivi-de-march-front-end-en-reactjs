import React, { useState } from 'react'
import Alert from 'react-bootstrap/Alert'
import { Button, Badge } from 'reactstrap';
import {Link} from 'react-router-dom';

const AlertComment = (props) => {
    const [show, setShow] = useState(false);
  
    return (
      <>
        <Alert show={show} variant="success" onClose={() => setShow(false)} >
          <span>
            Cette tache est une necessité
          </span>
         
          <div className="d-flex justify-content-end">
           <Link>
                <Badge onClick={() => setShow(false)} variant="outline-success">
                    Fermé
                </Badge>
            </Link>
          </div>
        </Alert>
  
        {!show && <Link><Badge color="secondary" onClick={() => setShow(true)}>Show Comments <Badge color="danger">5</Badge></Badge></Link>}
      </>
    );
  }
  
  export default AlertComment;