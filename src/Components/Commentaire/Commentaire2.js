import * as React from 'react';
import CommentsBlock from 'simple-react-comments';
//import { commentsData } from './data/index'; // Some comment data
 
class Commentaire2 extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      idTache : this.props.id_tache,
      comments: [],
      count: 0
    };
  }
 
  async componentDidMount() {

    const response = await fetch('/api/comments');
    const body = await response.json();
    let updated = [...body].filter(i =>(
        i.tache.id_tache === Number(this.state.idTache)));
    this.setState({comments : updated, count : updated.length});

}

  render() {
    return (
      <div>
        <CommentsBlock
          comments={this.state.comments}
          signinUrl={'/signin'}
          isLoggedIn
          reactRouter // set to true if you are using react-router
          onSubmit={text => {
            if (text.length > 0) {
              this.setState({
                comments: [
                  ...this.state.comments,
                  {
                    authorUrl: '#',
                    avatarUrl: '#avatarUrl',
                    createdAt: new Date(),
                    fullName: 'Name',
                    text,
                  },
                ],
              });
              console.log('submit:', text);
            }
          }}
        />
      </div>
    );
  }
}
 
export default Commentaire2;