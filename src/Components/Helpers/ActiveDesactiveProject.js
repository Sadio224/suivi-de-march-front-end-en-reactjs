import React, { Component } from 'react';
import CheckIcon from '@material-ui/icons/Check';
import ToggleButton from '@material-ui/lab/ToggleButton';

export default class StandaloneToggleButton extends Component {

    emptyItem = {
        intitule: "",
        datedebut: new Date,
        datefin: new Date,
        duree: "",
        etat: "invalide",
        projet: {
            id_projet: this.props.idProject,
        }
    }

    constructor(props){
        super(props)

        this.state = { 
            project : {},
            id_project : this.props.idProject,
            selected : false,
            Taches : [],
            item : this.emptyItem
        }
        this.updateTable= this.updateTable.bind(this);
    } 

  async updateTable (event) {


    this.setState({selected : !this.state.selected});

    const id = this.state.id_project
    const response= await fetch(`/api/projet/${id}`);
    const bodyProjet= await response.json();
    
    const responseTaches = await fetch('/api/taches');
    const bodyTaches = await responseTaches.json();
    this.setState({Taches : bodyTaches});

    if (this.state.selected === true){
        bodyProjet.etat = "valide"
    }
    else {
      bodyProjet.etat = "invalide"
    }
    await fetch(`/api/projet/${id}`, {
        method : 'PUT',
        headers : {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body : JSON.stringify(bodyProjet),
    });
    
    
    for (var i=0; i<this.state.Taches.length ; i++){
        await fetch(`/api/tache/${bodyTaches[i].id_tache}`, {
            method : 'PUT',
            headers : {
              'Accept': 'application/json',
              'Content-Type' : 'application/json'
            },
            body : JSON.stringify(bodyTaches[i]),
        });
    }
 
    window.location.reload(false);
      //event.peventDefault();
      //this.props.history.push("/projets");
  }

  async componentDidMount() {
    const id = this.state.id_project
    const response= await fetch(`/api/projet/${id}`);
    const body = await response.json();
    this.setState({project : body});
    const project = this.state.project;

    this.setState({
        selected : this.state.project.etat === "valide" ? true : false,
    })
  }
  

  render(){
    return (
        <ToggleButton
          value="check"
          selected={this.state.selected}
          onChange={this.updateTable}
          /*onChange={() => {
            this.setState({selected : !this.state.selected});
          }}*/
        >
          <CheckIcon />
        </ToggleButton>
      );
  }
}
