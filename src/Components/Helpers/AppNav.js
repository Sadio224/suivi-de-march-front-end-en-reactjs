import React, { Component } from 'react';
import  'bootstrap/dist/css/bootstrap.min.css' 
import {Nav,Navbar,NavItem,NavbarBrand, NavLink, UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem,
} from 'reactstrap';


class AppNav extends Component {
    state = {  }
    render() {
        return (
          <div>
            <Navbar color="dark" dark  expand="md">
              <NavbarBrand href="/">Suivi de Marché</NavbarBrand>
                <Nav className="ml-auto" navbar>
                  <NavItem>
                    <NavLink href="/">Home</NavLink>
                  </NavItem>
                  <NavItem>
                    <NavLink href="/services">Services</NavLink>
                  </NavItem>
                  <NavItem>
                    <NavLink href="/personnels">Personnels</NavLink>
                  </NavItem>
                  <NavItem>
                    <NavLink href="/projets">Projets</NavLink>
                  </NavItem>
                  <UncontrolledDropdown nav inNavbar>
                    <DropdownToggle nav caret>
                        Options
                    </DropdownToggle>
                    <DropdownMenu right>
                        <DropdownItem>
                        Option 1
                        </DropdownItem>
                        <DropdownItem>
                        Option 2
                        </DropdownItem>
                        <DropdownItem divider />
                        <DropdownItem>
                        Reset
                        </DropdownItem>
                    </DropdownMenu>
                    </UncontrolledDropdown>

                </Nav>
          
            </Navbar>
          </div>
        );
      }
}
 
export default AppNav;