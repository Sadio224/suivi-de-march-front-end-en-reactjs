import React from 'react'
import Breadcrumb from 'react-bootstrap/Breadcrumb'

const MyBreadcrumb = (props) => {
    return (
        <Breadcrumb>
            <Breadcrumb.Item href="/">Home</Breadcrumb.Item>
            <Breadcrumb.Item href="/service">Services</Breadcrumb.Item>
            <Breadcrumb.Item active>{props.name}</Breadcrumb.Item>
        </Breadcrumb>
    )
}

export default MyBreadcrumb;
