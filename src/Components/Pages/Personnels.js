import React, {Component} from 'react';


class Services extends Component {
    state = {
        isLoading : true,
        Personnels : []
    }

    async componentDidMount(){
        const response = await fetch('/api/personnels');
        const body = await response.json();
        this.setState({Personnels : body, isLoading : false});
    }

    render(){

        const {Personnels, isLoading} = this.state;
        if(isLoading)
            return (<div>Loading...</div>)
        return(
            <div>
                <h2>Personnels</h2>
                {
                    Personnels.map(personnel => 
                        <div id={personnel.id_personnel}>
                            {personnel.nom} {personnel.prenom}
                        </div>
                    )
                }
            </div>
        )
    }
}

export default Services;