import React, {Component} from 'react';


class Services extends Component {
    state = {
        isLoading : true,
        Services : []
    }

    async componentDidMount(){
        const response = await fetch('/api/services');
        const body = await response.json();
        this.setState({Services : body, isLoading : false});
    }

    render(){

        const {Services, isLoading} = this.state;
        if(isLoading)
            return (<div>Loading...</div>)
        return(
            <div>
                <h2>Services</h2>
                {
                    Services.map(service => 
                        <div id={service.id_service}>
                            {service.name_service}
                        </div>
                    )
                }
            </div>
        )
    }
}

export default Services;