import React, { Component } from 'react';

import  'bootstrap/dist/css/bootstrap.min.css' 
//import DatePicker from 'react-datepicker';
//import "react-datepicker/dist/react-datepicker.css";
import './ServiceAdd.css';
import {Badge, Table,Container,Input,Button,Label, FormGroup, Form} from 'reactstrap';
import {Link} from 'react-router-dom';
//import Moment from 'react-moment';

import Breadcrumbs from './Helpers/Breadcrumbs';

//import Modal from './Modal2'

class Services extends Component {

    emptyItem = {
        name_service: ''
    }

    
    constructor(props){
      super(props)

      this.state = { 
        isLoading :false,
        Services : [],
        Projects : [],
        Personnals : [],
        date :new Date(),
        item : this.emptyItem
       }

       this.handleSubmit= this.handleSubmit.bind(this);
       this.handleChange= this.handleChange.bind(this);
       this.handleDateChange= this.handleDateChange.bind(this);
       this.returnNbProject=this.returnNbProject.bind(this);
       this.returnNbPersonnal=this.returnNbPersonnal.bind(this);

    } 

    async handleSubmit(event){
     
      const item = this.state.item;
    

      await fetch(`/api/service`, {
        method : 'POST',
        headers : {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body : JSON.stringify(item),
      });
      
      event.peventDefault();
      this.props.history.push("/services");
    }


    handleChange(event){
      const target= event.target;
      const value= target.value;
      const name = target.name;
      let item={...this.state.item};
      item[name] = value;
      this.setState({item});
      console.log(item);
    }


    handleDateChange(date){
      let item={...this.state.item};
      item.expensedate= date;
      this.setState({item});
    
    }


    async remove(id){
        await fetch(`/api/service/${id}` , {
          method: 'DELETE' ,
          headers : {
            'Accept' : 'application/json',
            'Content-Type' : 'application/json'
          }

        }).then(() => {
          let updatedServices = [...this.state.Services].filter(i => i.id !== id);
          this.setState({Services : updatedServices});
        });

        window.location.reload(false);
    }


    async componentDidMount() {

        const response= await fetch('/api/services');
        const body= await response.json();
        this.setState({Services : body , isLoading :false});

        const responseProj= await fetch('/api/projets');
        const bodyProj= await responseProj.json();
        this.setState({Projects : bodyProj});

        const responsePers = await fetch('/api/personnels');
        const bodyPers = await responsePers.json();
        this.setState({Personnals : bodyPers});

    }

    returnNbProject(id_service){
        let updatedProjects = [...this.state.Projects].filter(i =>(
            i.serviceE.id_service === id_service));
        return updatedProjects.length;
    }

    returnNbPersonnal(id_service){
        let updatedPersonnals = [...this.state.Personnals].filter(i =>(
            i.service.id_service === id_service));
        return updatedPersonnals.length;
    }

    render() { 
        const title =<h3>Add Service</h3>;
        const {Services,isLoading} = this.state;
        

        if (isLoading)
            return(<div>Loading....</div>)
        
        
        let rows=
            Services.map( service =>
              <tr key={service.id_service}>
                <td><Link to={"services/"+service.id_service}>{service.name_service}</Link></td>
                <td>
                    <Badge color="info" outline>
                        Employers <Badge color="secondary">{this.returnNbPersonnal(service.id_service)}</Badge>
                    </Badge>
                </td>
                <td>
                    <Badge color="info" outline>
                        Projets <Badge color="secondary">{this.returnNbProject(service.id_service)}</Badge>
                    </Badge>
                </td>
                <td>
                    <Button className="button" color="primary" onClick={() => this.remove(service.id_service)}>Update</Button>
                    <Button className="button" color="danger" onClick={() => this.remove(service.id_service)}>Delete</Button>
                </td>
              </tr>
            )
        

        return (
            <div>
                <Breadcrumbs name="services"/>
                <Container>
                    {title}
                    
                    <Form onSubmit={this.handleSubmit}>
                        <FormGroup>
                            <Label for="name_service">Service Name</Label>
                            <Input type="name_service" name="name_service" id="name_service" 
                                onChange={this.handleChange} autoComplete="name"/>
                        
                        </FormGroup>

                        <FormGroup>
                            <Button color="primary" type="submit">Save</Button>{' '}
                            <Button color="secondary" to="/">Cancel</Button>
                        </FormGroup>
                    </Form>
                </Container>
              

          {''}
              <Container>
                <h3>Service List</h3>
                <Table className="mt-4">
                <thead>
                  <tr>
                    <th width="30%">Service Name</th>
                    <th width="25%">Nb Employers</th>
                    <th width="25%">Nb Projects</th>
                    <th width="20%">Action</th>
                  </tr>
                </thead>
                <tbody>
                   {rows}
                </tbody>

                </Table>
              </Container>

          }

        </div>

        );
    }
}
 
export default Services;
