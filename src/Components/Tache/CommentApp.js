import React, { Component } from "react";

//import "bootstrap/dist/css/bootstrap.css";


import CommentList from "./CommentairesTaches/CommentList";
import CommentForm from "./CommentairesTaches/CommentForm";

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      comments: [],
      loading: false,
      idTache : this.props.idTache
    };

    this.addComment = this.addComment.bind(this);
  }

  async componentDidMount() {

    const response = await fetch('/api/comments');
    const body = await response.json();
    let updated = [...body].filter(i =>(
        i.tache.id_tache === Number(this.state.idTache)));
    this.setState({comments : updated});

  }

  /**
   * Add new comment
   * @param {Object} comment
   */
  addComment(comment) {
    this.setState({
      loading: false,
      comments: [comment, ...this.state.comments]
    });
  }

  render() {
    const loadingSpin = this.state.loading ? "App-logo Spin" : "App-logo";
    return (
      <div className="App container bg-light shadow">
        <div className="row">
          <div className="col-4  pt-3 border-right">
            <h6>Que pensez vous ?</h6>
            <CommentForm addComment={this.addComment}  idTache={this.state.idTache}/>
          </div>
          <div className="col-8  pt-3 bg-white">
            <CommentList
              loading={this.state.loading}
              comments={this.state.comments}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default App;
