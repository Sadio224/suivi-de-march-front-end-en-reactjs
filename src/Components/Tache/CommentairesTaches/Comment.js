import React from "react";

import './Comment.css'

export default function Comment(props) {
  const { etat, message, time } = props.comment;

  return (
    <div className="media mb-3">

      <div className="media-body p-1 shadow-sm rounded border">
      <span className="float-left mt-0 mb-1 text-muted" >{etat}</span>
        <small className="float-right text-muted">{time}</small><br/>
      
        {message}
      </div>
    </div>
  );
}
