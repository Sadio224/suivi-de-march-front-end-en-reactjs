import React, { Component } from 'react'

import { Button,Label, FormGroup, Form } from 'reactstrap';
import DatePicker from 'react-datepicker';
import "react-datepicker/dist/react-datepicker.css";

const FormTache = (props) => {

    return (
        <div>
            <Form onSubmit={props.onSubmit}>
                <FormGroup>
                    <Label for="intitule">Intitule</Label>
                    <input className="input" type="intitule" name="intitule" id="intitule" onChange={props.onChange} autoComplete="name"/>
                </FormGroup>
                <FormGroup>
                    <Label for="city">Date debut</Label>
                    <DatePicker selected={props.selectedDateDebut}  onChange={props.onDateDebutChange} />
                </FormGroup>
                
                <FormGroup>
                    <Label for="city">Date fin</Label>
                    <DatePicker selected={props.selectedDateFin}  onChange={props.onDateFinChange} />
                </FormGroup>
                
                <FormGroup>
                    <Label for="duree">Duree</Label>
                    <input className="input" type="duree" name="duree" id="duree" onChange={props.onChange} autoComplete="name"/>
                </FormGroup>

                <FormGroup>
                    <Button color="primary" type="submit">Save</Button>{' '}
                    <Button color="secondary" to="/">Cancel</Button>
                </FormGroup>
            </Form>
        </div>
    );
}

export default FormTache;