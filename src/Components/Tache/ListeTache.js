import React, { Component } from 'react'
import {Badge, Table,Container,Button} from 'reactstrap';
import Comments from '../Commentaire/Commentaire'
import Comments2 from '../Commentaire/Commentaire2'

import Commentaire from './ModalCommentaire'

import '../ServiceAdd.css';

class ListeTache extends Component {
    state = { 
        idProject : this.props.idProject,
        Taches : [],
        Comments : []
    }

    async componentDidMount() {
        
        const response = await fetch('/api/taches');
        const body = await response.json();
        let updateProjects = [...body].filter(i => (
            i.projet.id_projet === Number(this.state.idProject)
        ));
        this.setState({Taches : updateProjects})
        
    }

    render() {

        const {Taches} = this.state;

        let rows=
        Taches.map( tache =>
              <tr key={tache.id_tache}>
                <td>{tache.intitule}<br/>
                   <Commentaire idTache={tache.id_tache} name={tache.intitule}/>
                </td>
                <td>{tache.datedebut}</td>
                <td>{tache.duree}</td>
                <td>{tache.etat ==="invalide" ? <Badge color="danger">{tache.etat}</Badge> : <Badge color="success">valide</Badge> } </td>
                <td>
                    <Button className="button" color="primary" onClick={() => this.remove(tache.id_tache)}>Update</Button>
                    <Button className="button" size="sm" color="danger" onClick={() => this.remove(tache.id_tache)}>Delete</Button>
                </td>
              </tr>
            )

            let thTache= (
                <tr>
                    <th width="30%">Intitule</th>
                    <th width="20%">Date debut</th>
                    <th width="15%">Duree</th>
                    <th width="10%">Etat</th>
                    <th width="25%">Action</th>
                </tr>
            )

        return ( 
            <Container>
                <div>
                    <span>Tache List</span>
                    <Table className="mt-4">
                        <thead>
                            {thTache}
                        </thead>
                        <tbody>
                            {rows}
                        </tbody>
                    </Table>
                </div>
            </Container>
        );
    }
}
 
export default ListeTache;