import React, { Component } from 'react';
import { Button, Badge, Modal, ModalHeader, ModalBody, ModalFooter} from 'reactstrap';
import  'bootstrap/dist/css/bootstrap.min.css' 
//import './Modal2.css'
import FormTache from './FormulaireTache'
import ListeTache from './ListeTache';
import {Link} from 'react-router-dom';

import CommentApp from './CommentApp'

class MyModalPers extends Component{


    emptyItem = {
        intitule: "",
        datedebut: new Date,
        datefin: new Date,
        duree: "",
        etat: "invalide",
        projet: {
            //id_projet: this.props.idProject,
        }
    }

    constructor(props){
        super(props)

        this.state = { 
          isLoading :false,
          item : this.emptyItem,
          modal : false,
          nestedModal : false,
          closeAll : false,
          ok : true,
          buttonFooter : "Add tache"
        }
  
         this.handleSubmit= this.handleSubmit.bind(this);
         this.handleChange= this.handleChange.bind(this);
         this.toggleAll=this.toggleAll.bind(this);
         this.toggle=this.toggle.bind(this);
         this.toggleNested=this.toggleNested.bind(this);
         this.handleDateChangeDebut= this.handleDateChangeDebut.bind(this);
         this.handleDateChangeFin= this.handleDateChangeFin.bind(this);
    } 

    
      handleChange(event){
        const target= event.target;
        const value= target.value;
        const name = target.name;
        let item={...this.state.item};
        item[name] = value;
        this.setState({item});
        console.log(item);
    }

    handleDateChangeDebut(date){
        let item={...this.state.item};
        item.datedebut= date;
        this.setState({item});
      
      }

      handleDateChangeFin(date){
        let item={...this.state.item};
        item.datefin= date;
        this.setState({item});
      
      }

    handleButtonChange = (e) => {
        this.setState({ok : false});
    }

    async handleSubmit(event){
     
        const item = this.state.item;
      
        await fetch(`/api/tache`, {
          method : 'POST',
          headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
          },
          body : JSON.stringify(item),
        });
        
        event.peventDefault();
        this.props.history.push(`/services/${this.props.idService}`);
    }

    toggle(event){
        const modal = this.state.modal;
        this.setState({modal : !modal});
    } 

    toggleNested(event){
        const nestedModal = this.state.nestedModal;
        this.setState({nestedModal : !nestedModal, closeAll : false });
    }

    toggleAll(event){
        const nestedModal = this.state.nestedModal;
        this.setState({nestedModal : !nestedModal, closeAll : true });
    }


    render(){

        return(
            <div>
                <span size="sm" color="primary" onClick={this.toggle}><Badge color="secondary">Commentaires</Badge> </span>
                <Modal isOpen={this.state.modal} toggle={this.toggle} className="col-md-10" size="lg" scrollable>
                    <ModalHeader toggle={this.toggle}>Tache : {this.props.name}
                    <span className="px-2" role="img" aria-label="Chat">
                        💬
                    </span>
                    </ModalHeader>
                    <ModalBody>
                       
                       <CommentApp name={this.props.name} idTache={this.props.idTache}/>

                    </ModalBody>
                    <ModalFooter>
                    <Button className="button" color="secondary" style={{width:'70px', height:'30px', fontSize:'13px'}} onClick={this.toggle}>Cancel</Button>
                    </ModalFooter>
                </Modal>
            </div>
        );
    }
}

export default MyModalPers

//<FormTache onSubmit={this.handleSubmit} onChange={this.handleChange} />